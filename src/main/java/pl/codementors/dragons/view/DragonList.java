package pl.codementors.dragons.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.dragons.DragonsDao;
import pl.codementors.dragons.EggsDao;
import pl.codementors.dragons.model.Dragon;
import pl.codementors.dragons.model.Egg;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Class for displaying list of dragons with list of eggs for each dragon. Implements Initializable interface.
 *
 * @author Paweł Wieraszka
 */
public class DragonList implements Initializable {
    private static final Logger log = Logger.getLogger(DragonList.class.getCanonicalName());

    /**
     * Table for displaying list of dragons.
     */
    @FXML
    private TableView<Dragon> dragonsTable;

    /**
     * Table for displaying list of eggs.
     */
    @FXML
    private TableView<Egg> eggsTable;

    /**
     * Name of dragon column.
     */
    @FXML
    private TableColumn<Dragon,String> nameColumn;

    /**
     * Color of dragon column.
     */
    @FXML
    private TableColumn<Dragon,String> colorColumn;

    /**
     * Wingspan of dragon column.
     */
    @FXML
    private TableColumn<Dragon,Integer> wingspanColumn;

    /**
     * Weight of egg column.
     */
    @FXML
    private TableColumn<Egg,Integer> weightColumn;

    /**
     * Diameter of egg column.
     */
    @FXML
    private TableColumn<Egg,Integer> diameterColumn;

    /**
     * Observable list of dragons.
     */
    private ObservableList<Dragon> dragons = FXCollections.observableArrayList();

    /**
     * Observable list of eggs.
     */
    private ObservableList<Egg> eggs = FXCollections.observableArrayList();

    /**
     * Resource bundle.
     */
    private ResourceBundle rb;

    /**
     * Selected dragon.
     */
    private ObjectProperty<Dragon> selectedDragon = new SimpleObjectProperty<>(null);

    /**
     * Object for managing dragons database.
     */
    private DragonsDao dao = new DragonsDao();

    /**
     * Object for managing eggs database.
     */
    private EggsDao eggsDao = new EggsDao();

    /**
     * Overwritten method from Initializable interface. Fills table with list of dragons and list of eggs.
     * Binds selected dragon with it's properties. Adds change listener to selected dragon for eggs list loading.
     * All columns are editable.
     * @param url
     * @param resourceBundle Bundle to be loaded.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        rb = resourceBundle;

        dragonsTable.setItems(dragons);
        eggsTable.setItems(eggs);

        selectedDragon.bind(dragonsTable.getSelectionModel().selectedItemProperty());

        selectedDragon.addListener(new ChangeListener<Dragon>() {
            @Override
            public void changed(ObservableValue<? extends Dragon> observableValue, Dragon oldValue, Dragon newValue) {
                eggs.clear();
                if (newValue != null) {
                    eggs.addAll(newValue.getEggs());
                }
            }
        });

        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setName(event.getNewValue());
            dao.merge(dragon);
        });

        colorColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        colorColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setColor(event.getNewValue());
            dao.merge(dragon);
        });

        wingspanColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        wingspanColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setWingspan(event.getNewValue());
            dao.merge(dragon);
        });

        weightColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        weightColumn.setOnEditCommit(event -> {
            Egg egg = event.getRowValue();
            egg.setWeight(event.getNewValue());
            dao.merge(egg.getDragon());
        });

        diameterColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        diameterColumn.setOnEditCommit(event -> {
            Egg egg = event.getRowValue();
            egg.setDiameter(event.getNewValue());
            dao.merge(egg.getDragon());
        });

        dragons.addAll(dao.findAll());
    }

    /**
     * Adds new dragon to the list and database.
     * @param event Press the button.
     */
    @FXML
    private void add(ActionEvent event) {
        Dragon dragon = new Dragon();
        dragon = dao.persist(dragon);
        dragons.add(dragon);
    }

    /**
     * Removes selected dragon from the list and database. Also removes all his eggs.
     * @param event Press the button.
     */
    @FXML
    private void remove(ActionEvent event) {
        if (selectedDragon.get() != null) {
            dao.delete(selectedDragon.get());
            dragons.remove(selectedDragon.get());
            dragonsTable.getSelectionModel().clearSelection();
        }
    }

    /**
     * Adds new egg to the list of selected dragon's eggs and to the database.
     * @param event  Press the button.
     */
    @FXML
    private void addEgg(ActionEvent event) {
        Dragon dragon = dragonsTable.getSelectionModel().getSelectedItem();
        if (dragon != null) {
            Egg egg = new Egg();
            egg.setDragon(dragon);
            egg = eggsDao.persist(egg);
            dragon.getEggs().add(egg);
            eggs.add(egg);
        }
    }

    /**
     * Removes egg from the list of selected dragon's eggs and from the database.
     * @param event Press the button.
     */
    @FXML
    private void removeEgg(ActionEvent event) {
        Egg egg = eggsTable.getSelectionModel().getSelectedItem();
        if (egg != null) {
            egg.getDragon().getEggs().remove(egg);
            eggs.remove(egg);
            dao.merge(egg.getDragon());
            eggsTable.getSelectionModel().clearSelection();
        }
    }
}
