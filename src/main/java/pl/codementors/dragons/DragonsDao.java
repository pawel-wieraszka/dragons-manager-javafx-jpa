package pl.codementors.dragons;

import pl.codementors.dragons.model.Dragon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Class for managing eggs database.
 */
public class DragonsDao {

    /**
     * Saves changes in dragons database.
     * @param dragon A dragon to be managed
     * @return Managed dragon.
     */
    public Dragon persist(Dragon dragon) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(dragon);
        tx.commit();
        em.close();
        return dragon;
    }

    /**
     * Updates changes in dragons database.
     * @param dragon A dragon to be managed.
     * @return Managed dragon.
     */
    public Dragon merge(Dragon dragon) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        dragon = em.merge(dragon);
        tx.commit();
        em.close();
        return dragon;
    }

    /**
     * Removes dragon from database.
     * @param dragon A dragon to be removed.
     */
    public void delete(Dragon dragon) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(dragon));
        tx.commit();
        em.close();
    }

    /**
     * Finds a dragon by id.
     * @param id Id of the dragon to be found.
     * @return Dragon by id.
     */
    public Dragon find(int id) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        Dragon dragon = em.find(Dragon.class, id);
        em.close();
        return dragon;
    }

    /**
     * Finds all dragons in database.
     * @return All dragons.
     */
    public List<Dragon> findAll() {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Dragon> query = cb.createQuery(Dragon.class);
        query.from(Dragon.class);
        List<Dragon> dragons = em.createQuery(query).getResultList();
        em.close();
        return dragons;
    }
}
