package pl.codementors.dragons.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Dragon class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "dragons")
public class Dragon {

    /**
     * Dragon's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Dragon's name.
     */
    @Column
    private String name;

    /**
     * Dragon's color.
     */
    @Column
    private String color;

    /**
     * Dragon's wingspan.
     */
    @Column
    private int wingspan;

    /**
     * Dragon's eggs.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dragon", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Egg> eggs = new ArrayList<>();

    public Dragon() {
    }

    public Dragon(String name, String color, int wingspan) {
        this.name = name;
        this.color = color;
        this.wingspan = wingspan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public List<Egg> getEggs() {
        return eggs;
    }

    public void setEggs(List<Egg> eggs) {
        this.eggs = eggs;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", wingspan=" + wingspan +
                '}';
    }
}
