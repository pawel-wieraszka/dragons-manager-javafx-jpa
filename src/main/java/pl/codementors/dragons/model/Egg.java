package pl.codementors.dragons.model;

import javax.persistence.*;

/**
 * Egg class.
 *
 * @author Paweł Wieraszka
 */
@Entity
@Table(name = "eggs")
public class Egg {

    /**
     * Egg's id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Egg's weight.
     */
    @Column
    private int weight;

    /**
     * Egg's diameter.
     */
    @Column
    private int diameter;

    /**
     * Dragon - owner of the egg.
     */
    @ManyToOne
    @JoinColumn(name = "dragon", referencedColumnName = "id")
    private Dragon dragon;

    public Egg() {
    }

    public Egg(int weight, int diameter, Dragon dragon) {
        this.weight = weight;
        this.diameter = diameter;
        this.dragon = dragon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public Dragon getDragon() {
        return dragon;
    }

    public void setDragon(Dragon dragon) {
        this.dragon = dragon;
    }

    @Override
    public String toString() {
        return "Egg{" +
                "id=" + id +
                ", weight=" + weight +
                ", diameter=" + diameter +
                '}';
    }
}
