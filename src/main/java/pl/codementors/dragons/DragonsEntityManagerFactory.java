package pl.codementors.dragons;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Class for creating Entity Manager Factory.
 *
 * @author Paweł Wieraszka
 */
public class DragonsEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("DragonsPU");

    /**
     * Creates Entity Manager.
     * @return Entity Manager.
     */
    public static final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Closes Entity Manager Factory.
     */
    public static void close() {
        emf.close();
    }
}
