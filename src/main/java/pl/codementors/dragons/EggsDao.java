package pl.codementors.dragons;

import pl.codementors.dragons.model.Egg;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Class for managing eggs database.
 */
public class EggsDao {

        /**
         * Saves changes in eggs database.
         * @param egg An egg to be managed
         * @return Managed egg.
         */
        public Egg persist(Egg egg) {
        EntityManager em = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(egg);
        tx.commit();
        em.close();
        return egg;
    }
}
