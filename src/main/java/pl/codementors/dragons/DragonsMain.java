package pl.codementors.dragons;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Main class of the application.
 *
 * @author Paweł Wieraszka
 */
public class DragonsMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starts the stage with the scene (800x600), but firstly loads fxml with javaFX view
     * and resource bundles. Close entity manager factory during window closing.
     * @param stage Stage to be shown.
     * @throws Exception
     */
    public void start(Stage stage) throws Exception {
        URL fxml = DragonsMain.class.getResource("/pl/codementors/jpa/dragons/view/dragon_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.jpa.dragons.messages.dragons_list_msg");

        FXMLLoader loader = new FXMLLoader(fxml, rb);
        Parent root = loader.load();

        Scene scene = new Scene(root, 800, 600);

        stage.setTitle(rb.getString("applicationTitle"));
        stage.setScene(scene);

        stage.setOnCloseRequest(event -> DragonsEntityManagerFactory.close());

        stage.show();
    }
}
